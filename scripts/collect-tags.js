const path = require('path');
const fs = require('fs');
const glob = require('glob');
const childProcess = require("child_process");

/**
 * Resolves the root-relative path to an absolute path.
 */
function resolvePath (p) {
    return path.resolve(__dirname, '..', p);
}

/**
 * Given a path, checks if there is a package.json file in and attempts to parse
 * it. Throws an error if it's not found, or malformed (not JSON).
 */
function readManifest (workspace) {
    const fullPath = path.join(workspace, 'package.json');
    const contents = fs.readFileSync(fullPath, 'utf-8');
    return JSON.parse(contents);
}

/**
 * Looks for the 'workspaces' field in an object and returns he patterns that
 * point to packages. Returns undefined if the field does not exist.
 */
function getWorkspaces (manifest) {
    if ('workspaces' in manifest) {
        const workspaces = manifest.workspaces;
        return (workspaces instanceof Array)
            ? workspaces
            : workspaces.packages;
    }
}

/**
 * Given a path to a lerna workspace, reads the manifest and retrieves the
 * version from it.
 */
function getVersionFromManifest (workspace) {
    try {
        const { version, name } = readManifest(workspace);
        return { version, name };
    } catch (e) {
        return false;
    }
}

/**
 * Returns a list of workspaces that need to be tagged for update. The list
 * contains objects with the path to the workspace (root relative) and the
 * version to create a tag for.
 */
function collectTags (manifest, marker) {
    const workspaces = getWorkspaces(manifest);
    if (workspaces) {
        const paths = workspaces.flatMap((pt) => glob.sync(pt));

        // For every workspace path, look for the marker file, and get the
        // version from manifest if the marker file is found.
        const findings = paths.map((p) => {
            if (fs.existsSync(resolvePath(path.join(p, marker)))) {
                const info = getVersionFromManifest(p);
                if (info) {
                    return Object.assign({ path: p }, info);
                }
            }
        });

        return findings.filter((found) => !!found);
    }
}

/**
 * Given a path to a monorepo, generates git tags with the given options for all
 * packages that desire so.
 */
function generateTags (monorepo, { annotate, cleanup, markerFilename }) {
    const manifest = readManifest(resolvePath(monorepo));
    const tags = collectTags(manifest, markerFilename);
    
    for (const tag of tags) {
        // Default arguments to the git tag command.
        const gitArgs = [
            `${tag.name}@${tag.version}` // the tag to create
        ]
    
        // Generate extra args if we want to annotate our tag.
        if (annotate) {
            gitArgs.push('-a');
            gitArgs.push(`-m "release: ${tag.name} ${tag.version}"`);
        }
    
        // Generate all tags.
        childProcess.execSync(`git tag ${gitArgs.join(' ')}`);
    
        // Remove all marker files found if so desired.
        if (cleanup) {
            fs.unlinkSync(path.join(tag.path, markerFilename));
        }
    }
}


/** Setup of the CLI tool. */

const cli = require('yargs');

cli.option('path', {
    alias: 'p',
    type: 'string',
    description: 'path to the monorepo to collect tags for',
    default: '.'
})

cli.option('annotate', {
    alias: 'a',
    type: 'boolean',
    description: 'annotate the generated tags'
});

cli.option('cleanup', {
    alias: 'c',
    type: 'boolean',
    description: 'remove the marker files after the tags have been generated'
});

cli.option('marker-filename', {
    alias: 'm',
    type: 'string',
    description: 'the filename of the marker files to look for',
    default: 'VERSION-TAG-ME'
});

cli.help('h');


/** Main program. Let's tie it all together! */

const options = cli.argv;
try {
    generateTags(options.path, options);
} catch (e) {
    console.log('\nCollecting tags failed. The error reported was this:');
    
    if (e.code === 'ENOENT') {
        // Nicely wrap the file not found error.
        console.error(`no package.json found in the given directory: ${resolvePath(options.path)}`);
    } else {
        // Something probably went wrong with the command.
        console.error(e.message);
    }
    
    process.exit(-1);
}
