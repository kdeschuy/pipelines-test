# Playing Around With Bitbucket Pipelines

In this repository I play around a bit with lerna-managed monorepo's and bitbucket pipelines to see what is possible.

## Helpful Stuff

### Bumping a package without creating a tag

Bumping a version of a workspace (or _package_) is as easy as going to the manifest file (or _package.json_) for that workspace and changing the `version` field there. However, in ci/cd environments, that's more easily said than done.

However, you can also bump the package version [using yarn](https://classic.yarnpkg.com/en/docs/cli/version/), like so:

```
yarn workspace <workspace name> version --no-git-tag-version [--major | --minor | --patch]
```

The `no-git-tag-version` can of course be skipped, but I'd rather create those tags myself. Also note that this command runs the appropriate [lifecycle scripts](https://classic.yarnpkg.com/en/docs/cli/version/#toc-version-lifecycle).

### Automatically create tags for all workspaces

I added a script that you can run from the root, which finds all workspaces (as declared in the root package.json) that have a certain 'marker file' in them, and generates git tags for these workspaces. The command in particular is called like so:

```
node scripts/collect-tags [-a|--annotate] [-c|--cleanup] [-m <name>|--marker-filename <name>]
```

The following options are supported:

`--annotate` _(alias: `-a`)_

By default, lightweight git tags are created. With this flag, the git tag will be annotated (and receives a message of the format `release: <name> <version>`).

`--cleanup` _(alias: `-c`)_

When enabled, removes all marker files from the appropriate workspaces after the tags have been created.

`--marker-filename` _(alias: `-`)_

The filename to look for in all workspaces. By default, the script looks for files named __VERSION-TAG-ME__ (no extension).

> In order to create a tag for a workspace, you need to create a marker files in the workspaces you wish to create tags for. I added a script in all packages that makes this easier. Just run `yarn workspaces run version-tag-me`.


## Experiments

### Experiment #1

The first configuration, introduced in this commit, is rather straightforward. We define a custom pipeline called `release`, which does the following:

- It merges a topic branch into the master branch and amends the merge message so it follows the commit convention.
- It leverages lerna to bump the version of changed packages using conventional commits. The commit message is also formatted in the same way. Changelogs are generated as well.
- It leverages lerna to publish the changed packages. Note that, for demonstration purposes, we don't actually publish anything in this repo.
- The branch that initiated the pipeline is deleted.

This way, the pipeline creates two commits: one for the merge, and one for the publish. Tags for the updated packages are also created (at the publish commit) by lerna.

To test this, we create two topic branches, and run the pipeline for them.

### Experiment #2 

I tried to see if it was possible to squash the merge and publish commits into one, somehow. Since there is an `--amend` flag in `lerna version`, I figured it'd be straightforward. However, this does not work, as somehow lerna doesn't want to push with both this flag and the custom message. Bummer.

I also create a branch with a breaking change here.

### Experiment #3

Using our collect-tags script, we can circumvent the issue in experiment #2! If lerna doesn't want to push its tags, we just generate are own! And push that!

Note that, in the previous experiment, the publish would have been done, but it would not be reflected in the git history. That's bad. Also, our remote feature branch has been removed! Extra bad!


## Conclusion

We have explored two ways to use bitbucket pipelines and lerna to automatically merge topic branches in the master branch, bumping the version of affected workspace and publishing them in the process. The first way, explored in experiment #1, creates two commits (merge and publish separate) and leverages lerna to create git tags. With the second way (broken version in experiment #2, working version in experiment #3), we use our own scripts to squash the merge and publish into a single commit.
