# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.1.1](https://bitbucket.org/kdeschuy/pipelines-test/compare/@kdeschuy/foo@0.1.0...@kdeschuy/foo@0.1.1) (2020-04-10)


### Bug Fixes

* updated the pipeline configuration to actually work now ([8e852da](https://bitbucket.org/kdeschuy/pipelines-test/commits/8e852dacd4f6bacccae0cd1185419c44edd0ceb1))






# [0.1.0](http://bitbucket.org/kdeschuy/pipelines-test/compare/@kdeschuy/foo@0.0.0...@kdeschuy/foo@0.1.0) (2020-04-10)


### Features

* added a new api method ([adbee6e](http://bitbucket.org/kdeschuy/pipelines-test/commits/adbee6e75c9b4a255de3e9b30b70020bd8a937e2))
