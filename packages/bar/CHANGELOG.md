# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.0.0](http://bitbucket.org/kdeschuy/pipelines-test/compare/@kdeschuy/bar@0.1.1...@kdeschuy/bar@1.0.0) (2020-04-10)


* feat!: updated the api in a breaking way ([3dc6195](http://bitbucket.org/kdeschuy/pipelines-test/commits/3dc619572714d336f227c6db48b65745f04b6f70))


### BREAKING CHANGES

* note that there is an exclamation mark to draw attention to the fact that this is a breaking change.





## [0.1.1](https://bitbucket.org/kdeschuy/pipelines-test/compare/@kdeschuy/bar@0.1.0...@kdeschuy/bar@0.1.1) (2020-04-10)


### Bug Fixes

* updated the pipeline configuration to actually work now ([8e852da](https://bitbucket.org/kdeschuy/pipelines-test/commits/8e852dacd4f6bacccae0cd1185419c44edd0ceb1))






# [0.1.0](http://bitbucket.org/kdeschuy/pipelines-test/compare/@kdeschuy/bar@0.0.0...@kdeschuy/bar@0.1.0) (2020-04-10)


### Bug Fixes

* made the api more awesome ([aed9f00](http://bitbucket.org/kdeschuy/pipelines-test/commits/aed9f00771714d61ad13ce2f8ba10f0f7ce51bde))


### Features

* added a new api method ([7558237](http://bitbucket.org/kdeschuy/pipelines-test/commits/7558237560bda97168e5dd58cc4e2a92972d8be1))
